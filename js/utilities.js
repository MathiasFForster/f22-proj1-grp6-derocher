/**
 * Either disables or enable the add project button
 * @param {Boolean} action_disable - Represents whether to disable or enable
 * 
 * Mathias
 */
let enable_disable_Button = (action_disable) => {

    let button_add = document.getElementsByTagName('button')[0];

    if(action_disable) {
        button_add.setAttribute('disabled','disabled'); 
    } else {
        button_add.removeAttribute('disabled'); 
    }
}

/**
 * Adds image to required element
 * @param {String} element_id - String of an element's id
 * @param {Object} image - Image object to be appended
 * 
 * Mathias
 */
function set_elem_required(element_id, image) {

    let element = document.getElementById(element_id);
    element.setAttribute('required', '');

    let labels = document.getElementsByTagName('label');
    let label = null;

    for(let i = 0; i < labels.length; i++) {
        if(labels[i].htmlFor == element_id) {
            label = labels[i];
        }
    }

    label.appendChild(image);
}

/**
 * Creates a table in parent element and fills it with projects from array
 * @param {Object} project_array - Array of projects used to create table
 * @param {String} parent_id - Id of parent element
 * 
 * Mathias, Michael
 */
function createTableFromArrayObjects(project_array, parent_id) {

    let parent = document.getElementById(parent_id);

    project_array.forEach(proj => {
        let row = document.createElement('tr');

        Object.values(proj).forEach(text => {
            let cell = document.createElement('td');
            let Txt_node = document.createTextNode(text);
            cell.appendChild(Txt_node);
            row.appendChild(cell);
        });
        
        let edit = document.createElement('td');
        img = document.createElement('img');
        img.src = '../images/edit.ico';
        img.alt = 'edit';
        img.addEventListener("click", function(event){
            editTableRow(event);
        });
        edit.appendChild(img);
        row.appendChild(edit);

        let del = document.createElement('td');
        img = document.createElement('img');
        img.src = '../images/delete.ico';
        img.alt = 'delete';
        img.addEventListener("click", function(event){
            deleteTableRow(event);
        });
        del.appendChild(img);
        row.appendChild(del);

        parent.appendChild(row);
        
        checkIfTableNull();
    });
}

/**
 * Tests if a string matches a regular expression
 * @param {Object} regex - Regular expression used for test 
 * @param {String} str - String to be tested 
 * @returns Boolean
 * 
 * Mathias
 */
function validateElement(regex, str) {
    
    if(!regex.test(str)) {
        return false;
    } else {
        return true;
    }
}

/**
 * Adds image to valid or invalid inputs
 * @param {String} element_id - Id of element that has been validated
 * @param {Boolean} validation_condition - Whether it is valid or invalid
 * @param {String} div_id - Id of parent div
 * 
 * Mathias
 */
function setElementFeedback(element_id, validation_condition, div_id) {

    let parent_div = document.getElementById(div_id);
    let message = document.createElement('p');
    message.textContent = 'wrong format for ' + element_id;

    let image_wrong = document.createElement("img");
    image_wrong.src = "../images/wrong.PNG";

    let image_valid = document.createElement("img");
    image_valid.src = "../images/valid.PNG";

    if(!validation_condition) {
        set_elem_required(element_id, image_wrong);
        parent_div.appendChild(message);
    } else {
        set_elem_required(element_id, image_valid);
    }

}

/**
 * Prevents error messages from repeating
 * @param {String} div_id - Id of parent div
 * @param {*} label_position - Position of label in list of labels
 * 
 * Mathias
 */
function repetition_avoidance(div_id, label_position) {
    
    if(document.getElementById(div_id).children[2] != null) {
        document.getElementById(div_id).children[2].remove();
    }

    if(document.getElementsByTagName('label')[label_position].childElementCount == 1) {
        document.getElementsByTagName('label')[label_position].childNodes[1].remove();
    }
}

/**
 * Checks if an input matches a regex and updates it's validity accordingly
 * @param {Object} regex - Regular expression for test 
 * @param {String} input_value - String to be tested
 * @param {Number} validation_index - Position in validation array
 * 
 * Mathias
 */
function validations_insertion(regex,input_value,validation_index) {

    if(validateElement(regex,input_value)) {
        validations[validation_index] = false;
    } else {
        validations[validation_index] = true;
    }
}

/**
 * Prevents repetition of error messages for list inputs
 * @param {String} div_id - Id of parent div 
 * @param {Number} label_position - Position of label
 * 
 * Mathias
 */
function repetition_avoidance_for_lists(div_id, label_position) {

    if(document.getElementById(div_id).children[3] != null) {
        document.getElementById(div_id).children[3].remove();
    }

    if(document.getElementsByTagName('label')[label_position].childElementCount == 1) {
        document.getElementsByTagName('label')[label_position].childNodes[1].remove();
    }
}

/**
 * Checks if there are any entries in the project table
 * and disables or enables save button depending on result
 * 
 * Michael
 */
function checkIfTableNull() {
    let saveButton = document.getElementById('save');
    if(all_projects_arr.length != 0){
        saveButton.removeAttribute('disabled');
    }
    else {
        saveButton.setAttribute('disabled','disabled');
    }
}

/**
 * Swaps the position of two elements in a given array
 * @param {NUmber} index1 - Index of first element to be swapped 
 * @param {Number} index2 - Index of second element to be swapped
 * @param {Object} array - Array where positions will be swapped 
 * @returns Object - Array with positions swapped
 * 
 * Michael
 */
function swapPositions(index1, index2, array){
    let tempElem = array[index1];
    array[index1] = array[index2];
    array[index2] = tempElem;

    return array;
}